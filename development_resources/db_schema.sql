--Create two new tables. One for wishers, one for wishes--
CREATE TABLE wishers(
 id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
 name CHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL UNIQUE,
 password CHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
);

CREATE TABLE wishes(
 id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 wisher_id INT NOT NULL,
 description CHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
 due_date DATE,
 FOREIGN KEY (wisher_id) REFERENCES wishers(id)
);
