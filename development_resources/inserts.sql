--Insert two wishers--
INSERT INTO wishers (name, password) 
 VALUES ('Tom', 'tomcat');
INSERT INTO wishers (name, password) 
 VALUES ('Jerry', 'jerrymouse');

--Insert four wishes, with ties to the above wishers--
INSERT INTO wishes (wisher_id, description, due_date) 
 VALUES (1, 'Sausage', 080401);
INSERT INTO wishes (wisher_id, description) 
 VALUES (1, 'Icecream');
INSERT INTO wishes (wisher_id, description, due_date) 
 VALUES (2, 'Cheese', 080501);
INSERT INTO wishes (wisher_id, description)
 VALUES (2, 'Candle');
