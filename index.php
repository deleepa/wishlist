<?php
require_once './includes/db.php';
//initialise variables
$logonSucess = false;
$username = "";
$password = "";

//check if the user accessed index.php from the same server
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];
    //method will return a boolean depending on the credentials
    $logonSucess = WishDB::getInstance()->verifyWisherCredentials($username, $password);
    //if logon is sucessful, redirect the user to editWishlist.php
    if ($logonSucess == true) {
        session_start();
        $_SESSION['user'] = $username;
        header('Location: editWishlist.php');
        exit;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title></title>
    </head>
    <body>
        <div id="headerContainer">
            <h1>Wishlist</h1>
        </div>
        <div id="indexMainContainer"> 
            <div id="showWishlist">
                <form name="wishList" action="wishlist.php" method="GET">
                    <p id="showWishlistText">Show wish list of: </p>
                    <div class="textInput" id="showWishlistInput">
                        <input type="text" name="user" value="" />
                    </div>
                    <div class="button" id="showWishlistSubmit">
                        <input type="submit" value="Go" />
                    </div>
                </form> 
            </div>
            <div id="logOn">
                <form name="logon" action="index.php" method="POST">
                    <p id="logOnUsername">Username: </p>
                    <div class="textInput" id="logOnUsernameInput">
                        <input type="text" name="username" value="" />
                    </div>    
                    <p id="logOnPassword">Password: </p>
                    <div class="textInput" id="logOnPasswordInput">
                        <input type="password" name="password" value="" />
                    </div>
                    <?php
                    //check if the user accessed index.php from the same server
                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        if (!$logonSucess) {
                            print " <div id='logOnError'><p>Invalid username or password</p></div>";
                        }
                    }
                    ?>
                    <div class="button" id="logOnSubmit">
                        <input type="submit" value="Edit my wishlist" />
                    </div>
                </form>
            </div>
        </div>
        <div id="createAccount">
            <p>Still don't have a wish list?! <a href='createNewWisher.php'>Create now</a></p>
        </div>
        
    </body>
</html>
