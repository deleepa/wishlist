<?php
    session_start();
    if (!array_key_exists("user", $_SESSION)) {
        header('Location: index.php');
        exit;
    }
    require_once './includes/db.php';
    /*
     * The values defined under the $wish array will depend on the 
     * way the user connects to the editWish.php.
     * There are three way the user will connect to this page:
     * 1. Add a new wish - 
     * 2. Edit an existing wish - $_GET
     * 3. An unsucessful save attempt - $_POST
     * Depending on the server request, the $wish array will be populated
     */
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        /*
         * If the user is redirected to this page due to an error in their submission,
         * the details that they had entered must be persisted and reloaded.
         * Create the $wish array and assign the values from the POST array.
         * They will be assigned to the fields at the bottom
         */

        $wish = array();
        $wish['wishID'] = $_POST['wishID'];
        $wish['description'] = $_POST['wish'];
        $wish['due_date'] = $_POST['dueDate'];
    }
    else if(array_key_exists("wishID", $_GET)) {
        /*
         * If the user is attempting to edit an individual wish,
         * then the request method will be GET. In that case populate the
         * $wish array from the database.
         */
        $wish = mysqli_fetch_array(WishDB::getInstance()->getWishByWishID($_GET['wishID']), MYSQLI_ASSOC);
    }
    else { 
        /*
         * If neither of those are true, then the page is loading for the first time as
         * add a new wish. Then the array should be populated with blank strings.
         */
        $wish = array();
        $wish['id'] = "";
        $wish['description'] = "";
        $wish['due_date'] = "";
    }

    $wisherID = WishDB::getInstance() -> getWisherIDByName($_SESSION['user']);
    $postedToDB = false;
    //used to check if the user has entered any information in the wish description
    $wishDescriptionIsEmpty = false;

    //check if the user has submitted the form
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        //check if the user has clicked the back button
        if(array_key_exists("back", $_POST) == true) {
            header('Location: editWishList.php');
            exit;
        }
        else {
            if($_POST['wish'] == "") {
                $wishDescriptionIsEmpty = true;
            }
            else if($_POST['wishID'] == "") {
                $wish = $_POST['wish'];
                $dueDate = $_POST['dueDate'];
                WishDB::getInstance() -> insertWish($wisherID, $wish, $dueDate);
                header('Location: editWishlist.php');
                exit;
            }
            
            else if($_POST['wishID'] != ""){
                $wishID = $_POST['wishID'];
                $wish = $_POST['wish'];
                $dueDate = $_POST['dueDate'];
                WishDB::getInstance() -> updateWish($wishID, $wish, $dueDate);
                header('Location: editWishlist.php');
                exit;
            }
            
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title></title>
    </head>
    <body>
        <div id="headerContainer">
            <h1>Wishlist</h1>
        </div>
        <div id='wishlistTitleContainer'>
            <p>Edit your wishes here <?php print $_SESSION['user']; ?></p>
        </div>
        <form name="editWish" action="editWish.php" method="POST">
            <div id="editWishMainBody">
                <p>Describe your wish:</p>
                <textarea name="wish"><?php print $wish['description'];?></textarea>
                <!--<input type="text" name="wish"  value="" />-->
                <p>When do you want to get it? (YYYY-MM-DD)</p>
                <input type="date" name="dueDate" value="<?php print $wish['due_date'];?>"/>
                <?php
                    if($wishDescriptionIsEmpty) {
                        print "Please enter description<br/>";
                    }
                ?>
            </div>
            <div id="userOptions">
                <input type="hidden" name="wishID" value="<?php print $wish['id']; ?>"/>
                <div id="saveWishSubmit">
                    <input type="submit" name="saveWish" value="Save Changes"/>
                </div>    
                <div id="backSubmit">                                
                    <input type="submit" name="back" value="Back to the List"/>
                </div>
            
                </form>
                <form name="backToMainPage" action="index.php">
                    <div id="editWishLogOutSubmit">
                        <input type="submit" value="Log out"/>
                    </div>
                </form>
            </div>
    </body>
</html>
