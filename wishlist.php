<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title></title>
    </head>
    <body>
        <div id="headerContainer">
            <h1>Wishlist</h1>
        </div>
        <div id="wishlistTitleContainer">
            <p>Wish list of <?php print $_GET['user']; ?> </p>
        </div>
        <div id="wishlistTableContainer">
            <?php
                require_once './includes/db.php';

                $requestedUser = $_GET['user'];

                $wisherID = WishDB::getInstance()->getWisherIDByName($requestedUser);

                if(!$wisherID) {
                    exit("The user $requestedUser was not found. Check the spelling and try again");
                }

                echo '<table id="wishlistTable">';
                echo '<tr>';
                echo '<th>description</th>';
                echo '<th>due_date</th>';
                echo '</tr>';
                $result = WishDB::getInstance()->getWishesByWisherID($wisherID);

                while (($row2 = mysqli_fetch_array($result, MYSQLI_ASSOC)) != NULL) {
                    echo '<tr>';
                    echo '<td>' . $row2['description'] . '</td>';
                    echo '<td>' . $row2['due_date'] . '</td>';
                    echo '</tr>';
                }
                mysqli_free_result($result);
                echo '</table>';
            ?>
        </div>
    </body>
</html>
