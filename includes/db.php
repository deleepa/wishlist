<?php

class WishDB extends mysqli {
    
    private static $instance = null;

    //db connection variables
    private $user = "root";
    private $password = "delee103";
    private $dbHost = "localhost";
    private $dbName = "wishDB";
    
//    private $user = "root";
//    private $password = "password";
//    private $dbHost = "localhost";
//    private $dbName = "wishlist";
    
    //return an instance of the object if its doesn't currently exist
    public static function getInstance() {
        //check if the $instance variable is an instance of self 
        if (!self::$instance instanceof self) {
            //if not instantiate an object of self in $instance
            self::$instance = new self;
        }
        //return $instance which should now have an object of self in it
        return self::$instance;
    }

    // The clone and wakeup methods prevents external instantiation of copies of the Singleton class,
    // thus eliminating the possibility of duplicate objects.
    public function __clone() {
      trigger_error('Clone is not allowed.', E_USER_ERROR);
    }
    public function __wakeup() {
      trigger_error('Deserializing is not allowed.', E_USER_ERROR);
    }
    
    //constructor
    private function __construct() {
        
        //similar to super();
        //calls the parent(mysqli) constructor
        parent::__construct($this->dbHost, $this->user, $this->password, $this->dbName);
        if(mysqli_connect_error()) {
            exit("Connection error" . mysqli_connect_errno());
        }
    }
    
    //function to get the wishers id after entering the correct name
    public function getWisherIDByName($name){
        //fixes the string for a mysql query (clears escape characters)
        $name = $this->real_escape_string($name);
        $queryStr = "SELECT id FROM wishers WHERE name='" . $name . "';";
        
        //query the db
        $result = $this->query($queryStr);
        
        //if there is a record under the given name
        //result will have more than 0 rows
        //`name` is UNIQUE
        if(($result->num_rows) > 0) {
            //puts the resulting row in a numbered array
            $row = $result->fetch_row();
            return $row[0];
        }
        else {
            return NULL;
        }
    }
    
    //get all the wishes under the given $wisherID
    public function getWishesByWisherID($wisherID) {
        
        //query string
        $queryStr = "SELECT id, description, due_date FROM wishes 
                                WHERE wisher_id='" . $wisherID . "';";
        
        //run the query and return a result set
        $wishersResults = $this->query($queryStr);
        
        return $wishersResults;
    }
    
    //create a wisher using the given parameters
    public function createWisher($name, $password) {
        //fixes the string for a mysql query (clears escape characters)
        $name = $this->real_escape_string($name);
        $password = $this->real_escape_string($password);
        
        $queryStr = "INSERT INTO wishers (name, password)
                            VALUES ('" . $name . "', '" . $password . "')";
        
        return $this->query($queryStr);
    }
    
    //check if the username and password entered are valid
    public function verifyWisherCredentials($username, $password) {
        //prep vars for query
        $username = $this->real_escape_string($username);
        $password = $this->real_escape_string($password);
        
        //query string
        $queryStr = "SELECT id FROM wishers WHERE name='".$username."' AND password='".$password."';";
        
        $result = $this->query($queryStr);
        $row = $result->fetch_row();
        
        if($row['0'] == null) {
            return false;
        }
        else {
            return true;
        }
    }
    
    //function to insert a new wish into the db
    public function insertWish($wisherID, $description, $dueDate){
        //check if the due date is null
        //if not it must be formatted properly first
        if($dueDate == null){
            $queryStr = "INSERT INTO wishes (wisher_id, description)
                                VALUES ('".$wisherID."', '".$description."')";
            $this->query($queryStr);
        }
        else {
            //$dueDate = $this->formatDueDate($dueDate);
            
            $queryStr = "INSERT INTO wishes (wisher_id, description, due_date)
                                VALUES ('".$wisherID."', '".$description."', '".$dueDate."')";
            $this->query($queryStr);
        }
    }
    
    public function formatDueDate($dueDate){
        $dateParts = date_parse($dueDate);
        return $dateParts["year"]*10000 + $dateParts["month"]*100 + $dateParts["day"];
    }
    
    //function to update the user's wish
    public function updateWish($wishID, $description, $duedate){
        
        $description = $this->real_escape_string($description);
        
        //if the duedate is null
        if ($duedate == '') {
            $queryStr = "UPDATE wishes SET description = '" . $description . "',
                 due_date = NULL WHERE id = " . $wishID;
            
            $this->query($queryStr);
        } 
        else {
            
            //$duedate = $this->formatDueDate($dueDate);
            
            $queryStr = "UPDATE wishes SET description = '" . $description .
                        "', due_date = '" . $duedate . "' WHERE id = " . $wishID;
            
            $this->query($queryStr);
        }
            
    } 
    
    public function getWishByWishID($wishID) {
        
        $queryStr = "SELECT id, description, due_date FROM wishes 
                     WHERE id='" . $wishID . "';";
        
        return $this->query($queryStr);
        
    }
    
    public function deleteWish($wishID){
        $queryStr = "DELETE FROM wishes WHERE id='".$wishID."'";
        
        $this->query($queryStr);
    }
    
    public function deleteUserAccount($wisherID) {
        $wisherID = $this->real_escape_string($wisherID);
        
        $queryStr = "DELETE FROM wishers WHERE id=".$wisherID.";";
        $queryStr2 = "DELETE FROM wishes WHERE wisher_id=".$wisherID.";";
        
        $result = $this->query($queryStr2);
        if($result) {
            $result = $this->query($queryStr);
            return $result;
        }
        else {
            return null;
        }
    }
}
?>