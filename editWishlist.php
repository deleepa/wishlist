<?php 
    session_start();
    require_once './includes/db.php';
    /*
     * The user must have an option to delete their account
     * When the delete my account button is pressed, they will
     * be brought to this page again. Here it will be checked if
     * the user has clicked the button. In that case, the 
     * deleteAccount method from db.php will be called and 
     * the user will be redirected to index.php
     */
    if(isset($_POST['deleteAccount'])) {
        //print "in if loop";
        //var_dump($_SESSION);
        //var_dump($_POST);
        
        $wisherID = WishDB::getInstance()->getWisherIDByName($_SESSION['user']);
        
        $result = WishDB::getInstance()->deleteUserAccount($wisherID);
        
        if($result){
            header("Location: index.php");
        }
        else {
            print "There was an error deleting your account. Please contact the administrator";
        }
        
    }
    
    if(isset($_POST['logOut'])) {
        session_destroy();
        header("Location: index.php");
    }



?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title></title>
    </head>
    <body>
        <div id="headerContainer">
            <h1>Wishlist</h1>
        </div>
        
        <?php
            if(array_key_exists("user", $_SESSION)){
                print " <div id='wishlistTitleContainer'>
                            <p>Hello " . $_SESSION['user']. "</p>
                        </div>";
                
                print "<div id='editWishlistTableContainer'>";
                
                print "<table id='editWishlistTable'>
                            <tr>
                                <th>Wish</th>
                                <th>Due Date</th>
                                <th></th>
                                <th></th>
                            </tr>";
                
                $wisherID = WishDB::getInstance()->getWisherIDByName($_SESSION['user']);
                $results = WishDB::getInstance()->getWishesByWisherID($wisherID);
                
                while(($row = mysqli_fetch_array($results)) != null) {
                    $wishID = $row['id'];
                    print "<tr>
                            <td>".$row['description']."</td>
                            <td>".$row['due_date']."</td>
                            <td>
                                <form name=\"editWish\" action=\"editWish.php\" method=\"GET\">
                                    <input type=\"hidden\" name=\"wishID\" value=\"".$wishID."\"/>
                                    <div class='editWishSubmit'>
                                        <input type=\"submit\" name=\"editWish\" value=\"Edit\"/>
                                    </div>
                                </form>
                            </td>
                            <td>
                                <form name=\"deleteWish\" action=\"deleteWish.php\" method=\"GET\">
                                    <input type=\"hidden\" name=\"wishID\" value=\"".$wishID."\"/>
                                    <div class='deleteWishSubmit'>
                                        <input type=\"submit\" name=\"deleteWish\" value=\"Delete\"/>
                                    </div>
                                </form>
                            </td>
                            </tr>";
                            
                }
                
                print "</table>";
                
                print "</div>";
                
                mysqli_free_result($results);
           
            } 
            
            //turn output buffering in php.ini to fix the issue.
            else {
                header('Location: index.php');
                exit;
            }
        ?>
        
        
        <div id="userOptions">
            <form name="logOut" action="#" method="POST">
                <div class="button" id="logOutSubmit">
                    <input type="submit" value="Log out" name="logOut"/>
                </div>
            </form>
            <form name="deleteAccount" action="#" method="POST">
                <div class="button" id="deleteAccountSubmit">
                    <input type="submit" value="Delete my account" name="deleteAccount"/>
                </div>
            </form>
            <form name="addNewWish" action="editWish.php">
                <div class="button" id="addWishSubmit">
                    <input type="submit" value="Add Wish" />
                </div>
            </form>
        </div>
    </body>
</html>
