<?php
    require_once './includes/db.php';

    //db connection credentials
//    $dbHost="localhost";
//    $dbUsername="root";
//    $dbPassword="password";
    
    $dbHost="localhost";
    $dbUsername="root";
    $dbPassword="delee103";
    $dbName = "wishDB";
    
    //other variables
    $userNameIsUnique = true;
    $passwordIsValid = true;				
    $userIsEmpty = false;					
    $passwordIsEmpty = false;				
    $password2IsEmpty = false;

    //check if the page was requested from its self
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        //check if the user typed in a user name
        if($_POST['user'] == "") {
            $userIsEmpty = TRUE;
        }

        //connect to the database
        $conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);
        if (!$conn) {
            die('Could not connect to MySQL: ' . mysqli_connect_error());

        }
        mysqli_query($conn, 'SET NAMES \'utf8\'');

        //variables from POST
        $requestedUsername = $_POST['user'];

        /* check if the username entered already exists by fetching the 
         * id number associated with the username
         */
        $result = WishDB::getInstance()-> getWisherIDByName($requestedUsername);
        //$foundID = mysqli_fetch_row($result);

        if($result != null){
            $userNameIsUnique = FALSE;
        }

        /* check that the user has entered a value in both 'password' and 
         * 'confirm password' fields.
         * then check if the two values match
         */
        if($_POST['password'] == ""){
            $passwordIsEmpty = TRUE;
        }
        if($_POST['password2'] == ""){
            $password2IsEmpty = TRUE;
        }
        if($_POST['password'] != $_POST['password2']){
            $passwordIsValid = FALSE;
        }

        /* check if all the conditions have been met.
         * if so add the 'name' and 'password' to the wishers db
         * then redirect the user to the editWishlist.php page
         */
        if($userNameIsUnique && !$userIsEmpty && $passwordIsValid &&
           !$passwordIsEmpty && !$password2IsEmpty) {

            $requestedPassword = $_POST['password'];

            $insert = WishDB::getInstance() -> createWisher($requestedUsername, $requestedPassword);

            if(!$insert) {
                exit("Fatal error occured" + mysqli_errno($conn));
            }

            session_start();
            $_SESSION['user'] = $requestedUsername;
            //var_dump($_SESSION);

            if($result != null) {
                mysqli_free_result($result);
            }
            
            mysqli_close($conn);
            header("Location: editWishlist.php"); //redirect browser
            exit();
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title></title>
    </head>
    <body>
         <div id="headerContainer">
            <h1>Wishlist</h1>
        </div>
        <div id='wishlistTitleContainer'>
            <p>Welcome!</p>
        </div>
        <div id="mainRegisterForm">
            <form name="newWisher" action="createNewWisher.php" method="POST">
                <p>Your name <input id="username" type="text" name="user" value="" /></p>
                <?php
                    if($userIsEmpty) {
                        print "<p id='error'>Please enter a valid username</p>";
                    }
                    if(!$userNameIsUnique){
                        print "<p id='error'>User already exists. Select a different name.</p>";
                    }
                ?>
                <p>Password <input id="password" type="password" name="password" value="" /></p>
                <?php
                    if($passwordIsEmpty) {
                        print "<p id='error'>Please enter a valid password</p>";
                    }
                ?>
                <p>Confirm password <input id="confirmPassword" type="password" name="password2" value="" /></p>
                <?php
                    if($password2IsEmpty) {
                        print "<p id='error'>Please confirm your password</p>";
                    }
                    if(!$password2IsEmpty && !$passwordIsValid){
                        print "<p id='error'>The passwords do not match.</p>";
                    }
                ?>
            </div>
            <div id="userOptions">
                <div id="registerSubmit">
                    <input type="submit" value="Register" />
                </div>
            </div>
        </form>
    </body>
</html>
